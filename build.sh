_pushd() {
	pushd $1 > /dev/null 2>&1
}
_popd() {
	popd > /dev/null 2>&1
}

BUILDOP=${1:-build}
echo "Running $BUILDOP ..."
_pushd cmd
for dir in router connector example-client-01; do
    _pushd $dir 
    go $BUILDOP
    _popd
done
_popd

