# crank4go-mini

A very small [Cranker](https://github.com/nicferrier/cranker), written for learning purposes.
See [crank4j](https://github.com/danielflower/crank4j/) for further context.

There are two pieces:

* The **router** accepts client HTTP requests
* The **connector** opens connections to the router, and then passes tunneled requests to target servers.

The connections look like this:

```
    Browser      |   DMZ        |    Internal Network
     GET   --------> router <-------- connector ---> HTTP Service
                 |              |
```

### Status

At this point, this code essentially demoes HTTP request proxying over websockets in Go -- for one special case.
The next step will be to implement the actual Cranker protocol.


### Build

Run `build.sh`.


### Run

1. Run `example-client-01`, which listens on port 3000.
2. Run `router`, which listens on port 8080.
3. Run `connector`, which listens on port 9090.
4. Open `http://localhost:9090` in your browser. This will get you results from `http://localhost:3000/time`.
