package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "demo.html")
	})

	http.HandleFunc("/time", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "%v\n", time.Now().Format(time.RFC1123))
	})

	fmt.Println("Listening on localhost:3000 ...")
	http.ListenAndServe(":3000", nil)
}
