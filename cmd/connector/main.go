// crank4go-mini connector

package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

func main() {
	socketUrl := "ws://localhost:8080" + "/register"
	conn, _, err := websocket.DefaultDialer.Dial(socketUrl, nil)
	if err != nil {
		log.Fatal("Error connecting to Websocket Server:", err)
	}
	defer conn.Close()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		err := conn.WriteMessage(websocket.TextMessage, []byte("/time"))
		if err != nil {
			log.Println("Error during writing to websocket:", err)
			return
		}
		_, msg, err := conn.ReadMessage()
		if err != nil {
			log.Println("Error in receive:", err)
			return
		}
		fmt.Fprintf(w, "connector: received: %s\n", msg)
	})

	http.ListenAndServe(":9090", nil)

}
