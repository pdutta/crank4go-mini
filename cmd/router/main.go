// crank4go-mini router

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func fetchWebResource() []byte {
	c := http.Client{Timeout: time.Duration(2) * time.Second}
	resp, err := c.Get("http://localhost:3000/time")
	if err != nil {
		fmt.Printf("http client error: %s", err)
		return []byte("error")
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	return body
}

func main() {
	http.HandleFunc("/register", func(w http.ResponseWriter, r *http.Request) {
		conn, _ := upgrader.Upgrade(w, r, nil) // error ignored for sake of simplicity

		for {
			// Read message from client
			msgType, msg, err := conn.ReadMessage()
			if err != nil {
				fmt.Println("error: ", err)
				return
			}

			if string(msg) == "/time" {
				// Print the message to the console
				fmt.Printf("%s sent: %s\n", conn.RemoteAddr(), string(msg))
				// Write message back to browser
				if err = conn.WriteMessage(msgType, fetchWebResource()); err != nil {
					return
				}
			}
		}
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Router running ...\n")
	})

	http.ListenAndServe(":8080", nil)
}
